# PHPimages


| Naam | PHP versie | Branch | Pull | Triggert |
| --- | --- | --- | --- | --- |
| php74 | 7.4 | [php74](https://gitlab.com/tjvb/phpimages/-/tree/php74)  |  `registry.gitlab.com/tjvb/phpimages:php74` | - |
| php80 | 8.0 | [php80](https://gitlab.com/tjvb/phpimages/-/tree/php80)  |  `registry.gitlab.com/tjvb/phpimages:php80` | php80_node |
| php80_node | 8.0 | [php80_node](https://gitlab.com/tjvb/phpimages/-/tree/php80_node)  |  `registry.gitlab.com/tjvb/phpimages:php80_node` | php80_puppeteer |
| php80_puppeteer | 8.0 | [php80_puppeteer](https://gitlab.com/tjvb/phpimages/-/tree/php80_puppeteer)  |  `registry.gitlab.com/tjvb/phpimages:php80_puppeteer` | - |
| php81 | 8.1 | [php81](https://gitlab.com/tjvb/phpimages/-/tree/php81)  |  `registry.gitlab.com/tjvb/phpimages:php81` | php81_node |
| php81_node | 8.1 | [php81_node](https://gitlab.com/tjvb/phpimages/-/tree/php81_node)  |  `registry.gitlab.com/tjvb/phpimages:php81_node` | php81_puppeteer |
| php81_puppeteer | 8.1 | [php81_puppeteer](https://gitlab.com/tjvb/phpimages/-/tree/php81_puppeteer)  |  `registry.gitlab.com/tjvb/phpimages:php81_puppeteer` | - |
| php82 | 8.2 | [php82](https://gitlab.com/tjvb/phpimages/-/tree/php82)  |  `registry.gitlab.com/tjvb/phpimages:php82` | php82_node |
| php82_node | 8.2 | [php82_node](https://gitlab.com/tjvb/phpimages/-/tree/php82_node)  |  `registry.gitlab.com/tjvb/phpimages:php82_node` | php82_puppeteer |
| php82_puppeteer | 8.2 | [php82_puppeteer](https://gitlab.com/tjvb/phpimages/-/tree/php82_puppeteer)  |  `registry.gitlab.com/tjvb/phpimages:php82_puppeteer` | - |
| php83 | 8.3 | [php83](https://gitlab.com/tjvb/phpimages/-/tree/php83)  |  `registry.gitlab.com/tjvb/phpimages:php83` | php83_node |
| php83_node | 8.3 | [php83_node](https://gitlab.com/tjvb/phpimages/-/tree/php83_node)  |  `registry.gitlab.com/tjvb/phpimages:php83_node` | php83_puppeteer |
| php83_puppeteer | 8.3 | [php83_puppeteer](https://gitlab.com/tjvb/phpimages/-/tree/php83_puppeteer)  |  `registry.gitlab.com/tjvb/phpimages:php83_puppeteer` | - |
| php84 | 8.4 | [php84](https://gitlab.com/tjvb/phpimages/-/tree/php84)  |  `registry.gitlab.com/tjvb/phpimages:php84` | php84_node |
| php84_node | 8.4 | [php84_node](https://gitlab.com/tjvb/phpimages/-/tree/php84_node)  |  `registry.gitlab.com/tjvb/phpimages:php84_node` | - |
